// Plugins
import vue from "@vitejs/plugin-vue";
import vuetify from "vite-plugin-vuetify";
import gzip from "rollup-plugin-gzip";

// Utilities
import { defineConfig } from "vite";
import { fileURLToPath, URL } from "node:url";

// https://vitejs.dev/config/,
export default defineConfig({
  plugins: [
    vue(),
    vuetify({
      autoImport: true,
    }),
    gzip(),
  ],
  base: "/fastman",
  build: {
    outDir: "dist",
    terserOptions: {
      ecma: 5,
      compress: true,
      sourceMap: false,
    },
    minify: "terser",
    rollupOptions: {
      output: {
        manualChunks: (id) => {
          if (id.includes("node_modules")) {
            const index = id.lastIndexOf("node_modules");
            const vendor = id.slice(index).split("/")[1];
            console.log(vendor);
            return vendor;
          }
        },
      },
    },
  },
  define: { "process.env": {} },
  resolve: {
    alias: {
      "@": fileURLToPath(new URL("./src", import.meta.url)),
    },
    extensions: [".js", ".json", ".jsx", ".mjs", ".ts", ".tsx", ".vue"],
  },
});
