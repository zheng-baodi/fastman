// Components

import { h, createApp, defineComponent, Suspense } from "vue";
import App from "./App.vue";

// Plugins
import { registerPlugins } from "@/plugins";

import "./app.css";

const AppWrapper = defineComponent({
  name: "AppWrapper",
  render() {
    return h(Suspense, {}, [h(App)]);
  },
});

const app = createApp(AppWrapper);

registerPlugins(app);

app.mount("#app");
