import { reactive } from "vue";
import { createGlobalState } from "@vueuse/core";
import { FormItemDisplay, OpAreaConfig } from "@/types";

const opAreaConfig = reactive<OpAreaConfig>({
  defaultRow: 7,
  defaultCol: 2,
  defaultAdditionsCount: 1,
  cellDisplay: FormItemDisplay.ALL,
});

const codeGenConfig = reactive<object>({});

export function initCodeGenConfig(config: object) {
  Object.assign(codeGenConfig, config);
}

export const useConfigs = createGlobalState(() => {
  return {
    opAreaConfig,
    codeGenConfig,
  };
});
