import { ParsedFormItem } from "./form";

export type CodeGenContextBuilder<
  C extends object = any,
  D extends Data = Data
> = (items: ParsedFormItem[], config: C) => D;

/** 内容为JS表达式/语句的字符串 */
export type ExpString = string;

/** 内容为模板字符串内容的字符串 */
export type TemplateString = string;

export type ArrayLiteralDesc = Array<
  ExpString | ObjectLiteralDesc | ArrayLiteralDesc
>;

export interface ObjectLiteralDesc {
  [key: string]: ExpString | ObjectLiteralDesc | ArrayLiteralDesc;
}

export const VNodeSymbol = Symbol("vnode");

/** 最终被用于生成代码的VNode */
export interface VNode {
  tag: string;
  props: {
    [key: string]: ExpString;
  };
  children: VNode[];
  readonly [VNodeSymbol]: true;
}

/** 类似VNode，但props的值并未被转为ExpString */
export type VNodeLike = {
  tag: string;
  props?: {
    [key: string]: any;
  };
  children?: VNodeLike[];
};

export interface ElementCodeGenContext {
  view: string;
  imports: string;
  formObjLiteral: string;
  componentRegisterObjLiteral: string;
  ruleObjLiteral: string;
}
