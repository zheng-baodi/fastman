import { ExpString } from "./generation";

export const enum FormItemDisplay {
  ALL,
  LABEL,
  FIELD,
}

export const enum TagClosingStyle {
  SingleTag = "<App />",
  CoupleTag = "<App></App>",
}

export const enum VueVModelSyntax {
  VModel = "v-model",
  Sync = "sync",
}

export const enum ColonType {
  Chinese = "：",
  English = ":",
  None = "",
}

export const enum TagNameStyle {
  Camel = "<MyApp />",
  Kebab = "<my-app />",
}

export const enum VueViewSyntax {
  Template = "template",
  JSX = "jsx",
}

export const enum VueComponetImport {
  All = "all",
  Custom = "custom",
  BuiltIn = "built-in",
  None = "none",
}

export interface OpAreaConfig {
  defaultRow: number;
  defaultCol: number;
  defaultAdditionsCount: number;
  cellDisplay: FormItemDisplay;
}

export interface BaseCodeGenConfig {
  TagClosingStyle: TagClosingStyle;
  colonType: ColonType;
  requirementPromptInfo: ExpString;
}

interface BasedVueCodeGenConfig extends BaseCodeGenConfig {
  formAccessor: string;
  vModelSyntax: VueVModelSyntax;
  importedComponents: VueComponetImport;
  tagNameStyle: TagNameStyle;
  viewSyntax: VueViewSyntax;
}

export interface ElementCodeGenConfig extends BasedVueCodeGenConfig {}
