import {
  ParsedComponent,
  ParsedRegularExp,
  ParsedValidator,
} from "./workspace";

export interface FormItem {
  label: string;
  field: string;
  required: boolean;
  component: string;
  regExps: string[];
  validators: string[];
  span: number;
}

export interface ParsedFormItem {
  label: string;
  field: string;
  required: boolean;
  component: ParsedComponent;
  regExps: ParsedRegularExp[];
  validators: ParsedValidator[];
  span: number;
}
