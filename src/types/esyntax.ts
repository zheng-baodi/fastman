export interface BaseImportDeclaration {
  source: string;
  specifiers: Array<
    ImportDefaultSpecifier | ImportSpecifier | ImportNamespaceSpecifier
  >;
}

export interface MixedImportDeclaration extends BaseImportDeclaration {
  specifiers:
    | [ImportDefaultSpecifier, ...ImportSpecifier[]]
    | ImportSpecifier[];
}

export interface NamespaceImportDeclaration extends BaseImportDeclaration {
  specifiers: [ImportNamespaceSpecifier];
}

export interface BaseImportSpecifier {
  type: string;
  local: string;
}

/** import A from 'A' */
export interface ImportDefaultSpecifier extends BaseImportSpecifier {
  type: "ImportDefaultSpecifier";
}

/** import { a as b } from 'A' */
export interface ImportSpecifier extends BaseImportSpecifier {
  type: "ImportSpecifier";
  imported: string;
}

/** import * as A from 'A' */
export interface ImportNamespaceSpecifier extends BaseImportSpecifier {
  type: "ImportNamespaceSpecifier";
}
