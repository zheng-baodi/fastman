import { BaseImportDeclaration } from "./esyntax";
import { CodeGenContextBuilder, TemplateString } from "./generation";
import { Component as VueComponent } from "vue";

export interface Resource {
  name: string;
  import: string;
}

export interface ParsedResource {
  id: string;
  name: string;
  import: BaseImportDeclaration;
}

interface BaseComponent {
  custom: boolean;
  props?: Record<string, any>;
  initialValue?: any;
}
export interface Component extends BaseComponent, Resource {}
export interface ParsedComponent extends BaseComponent, ParsedResource {}

export interface Validator extends Resource {}
export interface ParsedValidator extends ParsedResource {}

interface BaseRegularExp {
  message: TemplateString;
}
export interface RegularExp extends BaseRegularExp, Resource {}
export interface ParsedRegularExp extends BaseRegularExp, ParsedResource {}

export type Template = [name: string, path: string];
export type ParsedTemplate = {
  id: string;
  name: string;
  content: TemplateString;
};

export const enum UI {
  Unknown = "unknown",

  ElementUI = "element-ui",
  ElementPlus = "element-plus",

  // Antd = "antd",
  // AntdVue = "antd-vue"
}

export interface RawWorkspace {
  ui: UI;
  name: string;
  /** uid */
  path: string;

  components?: Component[];
  regExps?: RegularExp[];
  validators?: Validator[];
  templates: Template[];

  excludedBuiltInComponents?: string[];
  includedBuiltInComponents?: string[];
}

export type RawMultiWorkspace = string[];

/** ParsedWorkspace */
export interface Workspace {
  ui: UI;
  name: string;
  /** uid */
  path: string;

  components: ParsedComponent[];
  regExps: ParsedRegularExp[];
  validators: ParsedValidator[];
  templates: ParsedTemplate[];
}

export interface SystemDriver<C extends object = any, D extends Data = any> {
  type: UI;
  /** ui库的内置组件 */
  components: Component[];
  /** 获取代码生成上下文的函数 */
  buildCodeContext: CodeGenContextBuilder<C, D>;
  defaultCodeGenConfig: C;
  /** Vue组件——使用者以此更改代码生成配置 */
  configVueComp: VueComponent;
}
