import { FormItem, UI } from "@/types";
import { transformStringAsTemplate } from "@/utils";
import { normalizeFormItems } from "@/features";
import { mapToSystemDriver } from "./drivers";

export function generateCode({
  ui,
  items,
  template,
  config,
}: {
  ui: UI;
  items: FormItem[];
  template: string;
  config: object;
}) {
  const normalizedItems = normalizeFormItems(items);
  const sd = mapToSystemDriver(ui);
  // @ts-expect-error
  const ctx = sd.buildCodeContext(normalizedItems, config);
  return transformStringAsTemplate(template, ctx);
}
