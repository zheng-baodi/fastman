import {
  SystemDriver,
  ElementCodeGenConfig,
  UI,
  VueComponetImport,
  VueVModelSyntax,
  TagNameStyle,
  VueViewSyntax,
  ColonType,
  TagClosingStyle,
  ElementCodeGenContext,
} from "@/types";
import { v2, v3 } from "./components";
import { createElementBasedCCB } from "./ccbuilder";
import configVue from "./config.vue";

const baseConfig: ElementCodeGenConfig = {
  TagClosingStyle: TagClosingStyle.CoupleTag,
  colonType: ColonType.None,
  requirementPromptInfo: "请输入${label}",
  formAccessor: "form",
  vModelSyntax: VueVModelSyntax.VModel,
  importedComponents: VueComponetImport.None,
  tagNameStyle: TagNameStyle.Camel,
  viewSyntax: VueViewSyntax.JSX,
};

export const ElementUIDriver: SystemDriver<
  ElementCodeGenConfig,
  ElementCodeGenContext
> = {
  type: UI.ElementUI,
  components: v2,
  configVueComp: configVue,
  buildCodeContext: createElementBasedCCB(2),
  defaultCodeGenConfig: {
    ...baseConfig,
  },
};

export const ElementPlusDriver: SystemDriver<ElementCodeGenConfig, ElementCodeGenContext> = {
  type: UI.ElementPlus,
  components: v3,
  buildCodeContext: createElementBasedCCB(3),
  configVueComp: configVue,
  defaultCodeGenConfig: {
    ...baseConfig,
  },
};
