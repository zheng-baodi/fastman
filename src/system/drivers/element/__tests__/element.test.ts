import { v4 } from "uuid";
import { cloneDeep } from "lodash-es";
import { it, expect, describe } from "vitest";
import {
  TagClosingStyle,
  ColonType,
  TagNameStyle,
  VueComponetImport,
  VueVModelSyntax,
  VueViewSyntax,
  ParsedFormItem,
  ElementCodeGenConfig,
} from "@/types";
import { parseImportDeclaration } from "@/utils";
import { createElementBasedCCB } from "../ccbuilder";

const defaultConfig: ElementCodeGenConfig = {
  formAccessor: "form",
  colonType: ColonType.None,
  TagClosingStyle: TagClosingStyle.SingleTag,
  viewSyntax: VueViewSyntax.Template,
  tagNameStyle: TagNameStyle.Camel,
  vModelSyntax: VueVModelSyntax.VModel,
  importedComponents: VueComponetImport.All,
  requirementPromptInfo: "请输入${label}",
};

const parsedFormItem: ParsedFormItem = {
  label: "姓名",
  field: "name",
  required: true,
  span: 4,
  component: {
    id: v4(),
    custom: true,
    name: "测试",
    import: {
      source: "A",
      specifiers: [
        {
          type: "ImportDefaultSpecifier",
          local: "Input",
        },
      ],
    },
  },
  regExps: [],
  validators: [],
};

const buildCodeContext2 = createElementBasedCCB(2);
const buildCodeContext3 = createElementBasedCCB(3);

describe("element", () => {
  describe("TagClosingStyle", () => {
    it("couple", () => {
      const { view } = buildCodeContext3([parsedFormItem], {
        ...defaultConfig,
        TagClosingStyle: TagClosingStyle.CoupleTag,
      });
      expect(view).toBeExceptSpaces(`
        <ElRow>
          <ElCol :span="4">
            <ElFormItem label="姓名" prop="name"><Input v-model="form.name"></Input></ElFormItem>
          </ElCol>
        </ElRow>
    `);
    });

    it("single", () => {
      const { view } = buildCodeContext3([parsedFormItem], {
        ...defaultConfig,
        TagClosingStyle: TagClosingStyle.SingleTag,
      });
      expect(view).toBeExceptSpaces(`
        <ElRow>
          <ElCol :span="4">
            <ElFormItem label="姓名" prop="name"><Input v-model="form.name" /></ElFormItem>
          </ElCol>
        </ElRow>
    `);
    });
  });

  describe("colonType", () => {
    it("Chinese", () => {
      const { view } = buildCodeContext3([parsedFormItem], {
        ...defaultConfig,
        colonType: ColonType.Chinese,
      });
      expect(view).toBeExceptSpaces(`
        <ElRow>
          <ElCol :span="4">
            <ElFormItem label="姓名：" prop="name"><Input v-model="form.name" /></ElFormItem>
          </ElCol>
        </ElRow>
    `);
    });

    it("English", () => {
      const { view } = buildCodeContext3([parsedFormItem], {
        ...defaultConfig,
        colonType: ColonType.English,
      });
      expect(view).toBeExceptSpaces(`
        <ElRow>
          <ElCol :span="4">
            <ElFormItem label="姓名:" prop="name"><Input v-model="form.name" /></ElFormItem>
          </ElCol>
        </ElRow>
    `);
    });

    it("None", () => {
      const { view } = buildCodeContext3([parsedFormItem], {
        ...defaultConfig,
        colonType: ColonType.None,
      });
      expect(view).toBeExceptSpaces(`
        <ElRow>
          <ElCol :span="4">
            <ElFormItem label="姓名" prop="name"><Input v-model="form.name" /></ElFormItem>
          </ElCol>
        </ElRow>
    `);
    });
  });

  it("formAccessor", () => {
    const { view } = buildCodeContext3([parsedFormItem], {
      ...defaultConfig,
      formAccessor: "this.form",
    });
    expect(view).toBeExceptSpaces(`
      <ElRow>
        <ElCol :span="4">
          <ElFormItem label="姓名" prop="name"><Input v-model="this.form.name" /></ElFormItem>
        </ElCol>
      </ElRow>
  `);
  });

  describe("viewSyntax", () => {
    it("JSX v2", () => {
      const item = cloneDeep(parsedFormItem);
      item.component.props = {
        "props-a": 1,
      };
      const { view } = buildCodeContext2([item], {
        ...defaultConfig,
        viewSyntax: VueViewSyntax.JSX,
      });
      expect(view).toBeExceptSpaces(`
          <Row>
            <Col span={4}>
              <FormItem label="姓名" prop="name"><Input v-model={form.name} propsA={1} /></FormItem>
            </Col>
          </Row>
      `);
    });

    it("JSX v3", () => {
      const { view } = buildCodeContext3([parsedFormItem], {
        ...defaultConfig,
        viewSyntax: VueViewSyntax.JSX,
      });
      expect(view).toBeExceptSpaces(`
          <ElRow>
            <ElCol span={4}>
              <ElFormItem label="姓名" prop="name"><Input v-model={form.name} /></ElFormItem>
            </ElCol>
          </ElRow>
      `);
    });

    it("template", () => {
      const item = cloneDeep(parsedFormItem);
      item.component.props = {
        propsA: 1,
      };
      const { view } = buildCodeContext3([item], {
        ...defaultConfig,
        viewSyntax: VueViewSyntax.Template,
      });
      expect(view).toBeExceptSpaces(`
          <ElRow>
            <ElCol :span="4">
              <ElFormItem label="姓名" prop="name"><Input v-model="form.name" :props-a="1" /></ElFormItem>
            </ElCol>
          </ElRow>
      `);
    });
  });

  describe("tagNameStyle", () => {
    it("Kebab", () => {
      const { view } = buildCodeContext3([parsedFormItem], {
        ...defaultConfig,
        tagNameStyle: TagNameStyle.Kebab,
      });
      expect(view).toBeExceptSpaces(`
          <el-row>
            <el-col :span="4">
              <el-form-item label="姓名" prop="name"><input v-model="form.name" /></el-form-item>
            </el-col>
          </el-row>
      `);
    });

    it("Camel", () => {
      const { view } = buildCodeContext3([parsedFormItem], {
        ...defaultConfig,
        tagNameStyle: TagNameStyle.Camel,
      });
      expect(view).toBeExceptSpaces(`
          <ElRow>
            <ElCol :span="4">
              <ElFormItem label="姓名" prop="name"><Input v-model="form.name" /></ElFormItem>
            </ElCol>
          </ElRow>
      `);
    });
  });

  describe("vModelSyntax", () => {
    it("v-model", () => {
      const { view } = buildCodeContext3([parsedFormItem], {
        ...defaultConfig,
        vModelSyntax: VueVModelSyntax.VModel,
      });
      expect(view).toBeExceptSpaces(`
          <ElRow>
            <ElCol :span="4">
              <ElFormItem label="姓名" prop="name"><Input v-model="form.name" /></ElFormItem>
            </ElCol>
          </ElRow>
        `);
    });

    it("sync", () => {
      const { view } = buildCodeContext3([parsedFormItem], {
        ...defaultConfig,
        vModelSyntax: VueVModelSyntax.Sync,
      });
      expect(view).toBeExceptSpaces(`
        <ElRow>
          <ElCol :span="4">
            <ElFormItem label="姓名" prop="name"><Input :value.sync="form.name" /></ElFormItem>
          </ElCol>
        </ElRow>
      `);
    });
  });

  describe("importedComponents", () => {
    describe("v2", () => {
      it("None", () => {
        const { imports, componentRegisterObjLiteral } = buildCodeContext2(
          [parsedFormItem],
          {
            ...defaultConfig,
            importedComponents: VueComponetImport.None,
          }
        );
        expect(imports).toBeExceptSpaces("");
        expect(componentRegisterObjLiteral).toBeExceptSpaces("{}");
      });

      it("Custom", () => {
        const { imports, componentRegisterObjLiteral } = buildCodeContext2(
          [parsedFormItem],
          {
            ...defaultConfig,
            importedComponents: VueComponetImport.Custom,
          }
        );
        expect(imports).toBeExceptSpaces(`import Input from "A"`);
        expect(componentRegisterObjLiteral).toBeExceptSpaces(`{ Input }`);
      });

      it("All", () => {
        const item1 = cloneDeep(parsedFormItem);
        item1.component = {
          id: v4(),
          name: "输入框",
          custom: false,
          initialValue: "",
          import: parseImportDeclaration("import { Input } from 'element-ui'"),
        };
        const item2 = cloneDeep(parsedFormItem);
        item2.field = "foo";
        item2.component = {
          id: v4(),
          name: "自定义输入框",
          custom: true,
          initialValue: "",
          import: parseImportDeclaration("import { MyInput } from 'A'"),
        };
        const { view, imports, componentRegisterObjLiteral } =
          buildCodeContext2([item1, item2], {
            ...defaultConfig,
            tagNameStyle: TagNameStyle.Kebab,
            importedComponents: VueComponetImport.All,
          });
        expect(imports).toBeExceptSpaces(`
          import { Form, Row, Col, FormItem, Input } from "element-ui"
          import { MyInput } from "A"
        `);
        expect(componentRegisterObjLiteral).toBeExceptSpaces(
          `{ ElForm: Form, ElRow: Row, ElCol: Col, ElFormItem: FormItem, ElInput: Input, MyInput }`
        );
        expect(view).toBeExceptSpaces(`
          <el-row>
            <el-col :span="4">
              <el-form-item label="姓名" prop="name"><el-input v-model="form.name" /></el-form-item>
            </el-col>
            <el-col :span="4">
              <el-form-item label="姓名" prop="foo"><my-input v-model="form.foo" /></el-form-item>
            </el-col>
          </el-row>
        `);
      });

      it("BuiltIn", () => {
        const item = cloneDeep(parsedFormItem);
        item.component = {
          id: v4(),
          name: "输入框",
          custom: false,
          initialValue: "",
          import: parseImportDeclaration("import { Input } from 'element-ui'"),
        };
        const { view, imports, componentRegisterObjLiteral } =
          buildCodeContext2([item], {
            ...defaultConfig,
            tagNameStyle: TagNameStyle.Kebab,
            importedComponents: VueComponetImport.BuiltIn,
          });
        expect(imports).toBeExceptSpaces(
          `import { Form, Row, Col, FormItem, Input } from "element-ui"`
        );
        expect(componentRegisterObjLiteral).toBeExceptSpaces(
          `{ ElForm: Form, ElRow: Row, ElCol: Col, ElFormItem: FormItem, ElInput: Input }`
        );
        expect(view).toBeExceptSpaces(`
          <el-row>
            <el-col :span="4">
              <el-form-item label="姓名" prop="name"><el-input v-model="form.name" /></el-form-item>
            </el-col>
          </el-row>
        `);
      });
    });

    describe("v3", () => {
      it("None", () => {
        const { imports, componentRegisterObjLiteral } = buildCodeContext3(
          [parsedFormItem],
          {
            ...defaultConfig,
            importedComponents: VueComponetImport.None,
          }
        );
        expect(imports).toBeExceptSpaces("");
        expect(componentRegisterObjLiteral).toBeExceptSpaces("{}");
      });

      it("Custom", () => {
        const { imports, componentRegisterObjLiteral } = buildCodeContext3(
          [parsedFormItem],
          {
            ...defaultConfig,
            importedComponents: VueComponetImport.Custom,
          }
        );
        expect(imports).toBeExceptSpaces(`import Input from "A"`);
        expect(componentRegisterObjLiteral).toBeExceptSpaces(`{ Input }`);
      });

      it("All", () => {
        const { imports, componentRegisterObjLiteral } = buildCodeContext3(
          [parsedFormItem],
          {
            ...defaultConfig,
            importedComponents: VueComponetImport.All,
          }
        );
        expect(imports).toBeExceptSpaces(`
            import { ElForm, ElRow, ElCol, ElFormItem } from "element-plus"
            import Input from "A"
          `);
        expect(componentRegisterObjLiteral).toBeExceptSpaces(
          `{ ElForm, ElRow, ElCol, ElFormItem, Input }`
        );
      });

      it("BuiltIn", () => {
        const { imports, componentRegisterObjLiteral } = buildCodeContext3(
          [parsedFormItem],
          {
            ...defaultConfig,
            importedComponents: VueComponetImport.BuiltIn,
          }
        );
        expect(imports).toBeExceptSpaces(
          `import { ElForm, ElRow, ElCol, ElFormItem } from "element-plus"`
        );
        expect(componentRegisterObjLiteral).toBeExceptSpaces(
          `{ ElForm, ElRow, ElCol, ElFormItem }`
        );
      });
    });
  });

  it("componentRegisterObjLiteral", () => {
    const item1 = cloneDeep(parsedFormItem);
    item1.component = {
      id: v4(),
      name: "输入框1",
      custom: true,
      import: parseImportDeclaration(
        'import Input1, { a, b, c } from "element-plus"'
      ),
    };
    const item2 = cloneDeep(parsedFormItem);
    item2.component = {
      id: v4(),
      name: "输入框2",
      custom: true,
      import: parseImportDeclaration(
        'import { Input2, b, c } from "element-plus"'
      ),
    };
    const item3 = cloneDeep(parsedFormItem);
    item3.component = {
      id: v4(),
      name: "单选框",
      custom: false,
      import: parseImportDeclaration(
        'import { ElRadioGroup, ElRadio } from "element-plus"'
      ),
    };
    const { componentRegisterObjLiteral } = buildCodeContext3(
      [item1, item2, item3],
      {
        ...defaultConfig,
        importedComponents: VueComponetImport.All,
      }
    );
    expect(componentRegisterObjLiteral).toBeExceptSpaces(`
        { ElForm, ElRow, ElCol, ElFormItem, ElRadioGroup, ElRadio, Input1, Input2 }
      `);
  });

  it("required", () => {
    const item1 = cloneDeep(parsedFormItem);
    item1.label = "label1";
    item1.field = "field1";
    const item2 = cloneDeep(parsedFormItem);
    item2.label = "label2";
    item2.field = "field2";
    const item3 = cloneDeep(parsedFormItem);
    item3.label = "label3";
    item3.field = "field3";
    item3.required = false;
    const { ruleObjLiteral } = buildCodeContext3(
      [item1, item2, item3],
      defaultConfig
    );
    expect(ruleObjLiteral).toBeExceptSpaces(`
      {
        field1: [{required: true, message: "请输入label1" }],
        field2: [{required: true, message: "请输入label2" }]
      }
    `);
  });

  it("regExps", () => {
    const d1 = 'import patternA from "A"';
    const d2 = 'import patternB from "B"';
    const regExp1 = parseImportDeclaration(d1);
    const regExp2 = parseImportDeclaration(d2);
    const { imports, ruleObjLiteral } = buildCodeContext3(
      [
        {
          ...parsedFormItem,
          required: false,
          regExps: [
            {
              id: v4(),

              name: "数字验证",
              import: regExp1,
              message: "${label}需为数字",
            },
            {
              id: v4(),

              name: "test",
              import: regExp2,
              message: "test",
            },
          ],
        },
      ],
      defaultConfig
    );
    expect(imports).toMatch(d1);
    expect(imports).toMatch(d2);
    expect(ruleObjLiteral).toBeExceptSpaces(`
        { name: [{ pattern: patternA, message: "姓名需为数字" }, { pattern: patternB, message: "test" } ]}
      `);
  });

  it("validators", () => {
    const d1 = 'import validatorA from "A"';
    const d2 = 'import validatorB, { C } from "B"';
    const regExp1 = parseImportDeclaration(d1);
    const regExp2 = parseImportDeclaration(d2);
    const { imports, ruleObjLiteral } = buildCodeContext3(
      [
        {
          ...parsedFormItem,
          required: false,
          validators: [
            {
              id: v4(),
              name: "1",
              import: regExp1,
            },
            {
              id: v4(),
              name: "2",
              import: regExp2,
            },
          ],
        },
      ],
      defaultConfig
    );
    expect(imports).toMatch(d1);
    expect(imports).toMatch(d2);
    expect(ruleObjLiteral).toBeExceptSpaces(`
        { name: [{ validator: validatorA }, { validator: validatorB } ]}
      `);
  });

  it("requirementPromptInfo", () => {
    const item = cloneDeep(parsedFormItem);
    item.label = "label";
    const { ruleObjLiteral } = buildCodeContext3([item], {
      ...defaultConfig,
      requirementPromptInfo: "${label}不能为空",
    });
    expect(ruleObjLiteral).toBeExceptSpaces(`
        {
          name: [{required: true, message: "label不能为空" }]
        }
      `);
  });

  it("computed field", () => {
    const item = cloneDeep(parsedFormItem);
    item.field = "test-123";
    const { view } = buildCodeContext3([item], defaultConfig);
    expect(view).toBeExceptSpaces(`
        <ElRow>
          <ElCol :span="4">
            <ElFormItem label="姓名" prop="test-123"><Input v-model="form['test-123']" /></ElFormItem>
          </ElCol>
        </ElRow>
      `);
  });
});
