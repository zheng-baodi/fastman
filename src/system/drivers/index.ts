import { defineComponent, h } from "vue";
import { CodeGenContextBuilder, SystemDriver, UI } from "@/types";
import { ElementPlusDriver, ElementUIDriver } from "./element";

const DefaultDriver: SystemDriver = {
  type: UI.Unknown,
  configVueComp: defineComponent({
    name: "UnknownUIConfig",
    render() {
      return h("div", "加载失败");
    },
  }),
  defaultCodeGenConfig: {},
  components: [],
  buildCodeContext: (() => ({})) as CodeGenContextBuilder,
};

export function mapToSystemDriver(ui: UI) {
  switch (ui) {
    case UI.ElementUI:
      return ElementUIDriver;
    case UI.ElementPlus:
      return ElementPlusDriver;
    default:
      return DefaultDriver;
  }
}
