// 配置文件格式验证
import { UI } from "@/types";
import Joi from "joi";

const multipleWorkspaceConfigSchema = Joi.array()
  .items(Joi.string().label("workspace path"))
  .min(1)
  .label("workspace paths");

const resourceSchema = {
  name: Joi.string().required(),
  import: Joi.string().required(),
};

const validatorSchema = resourceSchema;
const regExpSchema = Joi.object({
  ...resourceSchema,
  message: Joi.string().required(),
});

const componentResourceSchema = Joi.object({
  ...resourceSchema,
  initialValue: Joi.any(),
  props:Joi. object(),
}).unknown(true);

const templateSchema = Joi.array().items(Joi.string()).length(2);

const singleWorkspaceConfigSchema = Joi.object({
  ui: Joi.string()
    .equal(...[UI.ElementUI, UI.ElementPlus])
    .required(),
  name: Joi.string().required(),
  includedBuiltInComponents: Joi.array().items(Joi.string()),
  excludedBuiltInComponents: Joi.array().items(Joi.string()),
  components: Joi.array().items(componentResourceSchema),
  regExps: Joi.array().items(regExpSchema),
  validators: Joi.array().items(validatorSchema),
  templates: Joi.array().items(templateSchema).min(1).required(),
}).unknown(true);

export default function validate(data: unknown, type: "multiple" | "single") {
  Joi.assert(
    data,
    type === "multiple"
      ? multipleWorkspaceConfigSchema
      : singleWorkspaceConfigSchema
  );
}
