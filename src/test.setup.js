import { expect } from 'vitest'

const filterSpace = (str) => str.replaceAll(/(\s|\\n)/g, "");

expect.extend({
  toBeExceptSpaces: (received, expected) => {
    received = filterSpace(`${received}`)
    expected = filterSpace(`${expected}`)

    if (received !== expected) {
      return {
        message: () => `expected ${received} to be ${expected} without spaces anywhere`,
        pass: false,
      }
    }

    return {
      pass: true
    }
  },
})
