import __Toast from "awesome-toast-component";

interface Toast {
  success: (msg: string) => __Toast;
  fail: (msg: string) => __Toast;
}

export const toast: Toast = {
  success(msg) {
    return new __Toast(msg, {
      position: "top",
      theme: "light",
      timeout: 2000,
      style: {
        container: [
          ["background-color", "#E8F5E9"],
          ["border-radius", "6px"],
          ["max-width", "unset"],
        ],
        message: [["color", "#66BB6A"]],
      },
    });
  },
  fail(msg) {
    return new __Toast(msg, {
      position: "top",
      timeout: 10000,
      style: {
        container: [
          ["background-color", "#F44336"],
          ["border-radius", "6px"],
          ["max-width", "unset"],
        ],
        message: [["color", "#fff"]],
      },
    });
  },
};
