import { it, expect, describe } from "vitest";
import { transformStringAsTemplate } from "@/utils";
import { camelCase, kebabCase } from "lodash-es";

describe("base tools", () => {
  it("transformStringAsTemplate", () => {
    const ctx = {
      a: 1,
      b: 2,
    };
    expect(() => transformStringAsTemplate("``", ctx)).toThrowError();
    expect(transformStringAsTemplate("${a}\\`${b}\\${a}", ctx)).toBe("1`2${a}");
  });
});
