import { describe, expect, it } from "vitest";
import {
  isIdentifier,
  isStringLiteral,
  isBooleanLiteral,
  mergeImportDeclarations,
  parseImportDeclaration,
  getFirstVariable,
  getAllVariables,
  hasImportDefaultSpecifier,
  isNamespaceDeclaration,
} from "../esyntax";
import {
  BaseImportDeclaration,
  ImportNamespaceSpecifier,
  ImportDefaultSpecifier,
  ImportSpecifier,
} from "@/types";

const importSyntaxErrorMsgReg = /非法的import语句/;
const variableSyntaxErrorMsgReg = /非法的变量/;

describe("esyntax", () => {
  it("isStringLiteral", () => {
    expect(isStringLiteral(`"abc"`)).toBe(true);
    expect(isStringLiteral(`'abc'`)).toBe(true);
    expect(isStringLiteral("`${window}`")).toBe(false);
    expect(isStringLiteral("obj.foo")).toBe(false);
    expect(isStringLiteral(`"name'`)).toBe(false);
  });

  it("isIdentifier", () => {
    expect(isIdentifier("1")).toBe(false);
    expect(isIdentifier("n")).toBe(true);
    expect(isIdentifier("obj.name")).toBe(false);
    expect(isIdentifier("obj-name")).toBe(false);
  });

  it("isBooleanLiteral", () => {
    expect(isBooleanLiteral("true")).toBe(true);
    expect(isBooleanLiteral("false")).toBe(true);
    expect(isBooleanLiteral("[]")).toBe(false);
  });

  it("hasImportDefaultSpecifier", () => {
    const i1 = parseImportDeclaration("import A from 'a'");
    const i2 = parseImportDeclaration("import A, { a, b } from 'a'");
    const i3 = parseImportDeclaration("import * as A from 'a'");
    const i4 = parseImportDeclaration("import { a, b } from 'a'");
    expect(hasImportDefaultSpecifier(i1)).toBe(true);
    expect(hasImportDefaultSpecifier(i2)).toBe(true);
    expect(hasImportDefaultSpecifier(i3)).toBe(false);
    expect(hasImportDefaultSpecifier(i4)).toBe(false);
  });

  it("isNamespaceDeclaration", () => {
    const i1 = parseImportDeclaration("import A from 'a'");
    const i2 = parseImportDeclaration("import A, { a, b } from 'a'");
    const i3 = parseImportDeclaration("import * as A from 'a'");
    const i4 = parseImportDeclaration("import { a, b } from 'a'");
    expect(isNamespaceDeclaration(i1)).toBe(false);
    expect(isNamespaceDeclaration(i2)).toBe(false);
    expect(isNamespaceDeclaration(i3)).toBe(true);
    expect(isNamespaceDeclaration(i4)).toBe(false);
  });

  it("getVariables", () => {
    expect(
      getAllVariables(parseImportDeclaration('import A, { a, b, c } from "A"'))
    ).toEqual(["A", "a", "b", "c"]);
    expect(
      getAllVariables(parseImportDeclaration('import {} from "A"'))
    ).toEqual([]);
  });

  it("getFirstVariable", () => {
    expect(
      getFirstVariable(parseImportDeclaration('import A, { a, b, c } from "A"'))
    ).toBe("A");
    expect(getFirstVariable(parseImportDeclaration('import {} from "A"'))).toBe(
      null
    );
  });

  it('import * as A from "A"', () => {
    const partial = (s: string) => () => parseImportDeclaration(s);

    expect(partial("import")).toThrowError(importSyntaxErrorMsgReg);
    expect(partial('import from "A"')).toThrowError(importSyntaxErrorMsgReg);
    expect(partial('import * as 123 from "A"')).toThrowError(
      variableSyntaxErrorMsgReg
    );
    const { source, specifiers } = parseImportDeclaration(
      'import * as A from "A"'
    );
    expect(source).toBe("A");

    expect(specifiers).toEqual([
      {
        type: "ImportNamespaceSpecifier",
        local: "A",
      } as ImportNamespaceSpecifier,
    ]);

    // 边界情况
    expect(partial(`import*as A from"A"`)).not.toThrow();
    expect(partial(`import * as A from"A";`)).not.toThrow();
    expect(partial(`  import*as A from"A";  `)).not.toThrow();
  });

  it('import A, { a, b } from "A"', () => {
    const partial = (s: string) => () => parseImportDeclaration(s);
    expect(partial("import")).toThrowError(importSyntaxErrorMsgReg);
    expect(partial('import from "A"')).toThrowError(importSyntaxErrorMsgReg);
    expect(partial('import from "{}"')).toThrowError(importSyntaxErrorMsgReg);
    expect(partial('import 1, {} from "A"')).toThrowError(
      variableSyntaxErrorMsgReg
    );
    expect(partial('import { a as 1 } from "A"')).toThrowError(
      variableSyntaxErrorMsgReg
    );
    expect(partial('import { 1 as a } from "A"')).toThrowError(
      variableSyntaxErrorMsgReg
    );
    expect(partial('import { a As b } from "A"')).toThrowError(
      variableSyntaxErrorMsgReg
    );

    const d1 = parseImportDeclaration('import A from "A"');
    expect(d1.source).toBe("A");
    expect(d1.specifiers).toEqual([
      { type: "ImportDefaultSpecifier", local: "A" } as ImportDefaultSpecifier,
    ]);

    const d2 = parseImportDeclaration('import { a, b as b1, c } from "A"');
    expect(d2.specifiers).toEqual([
      {
        type: "ImportSpecifier",
        imported: "a",
        local: "a",
      },
      {
        type: "ImportSpecifier",
        imported: "b",
        local: "b1",
      },
      {
        type: "ImportSpecifier",
        imported: "c",
        local: "c",
      },
    ] as ImportSpecifier[]);

    const d3 = parseImportDeclaration('import A, { a, b } from "A"');
    expect(d3.specifiers).toEqual([
      {
        type: "ImportDefaultSpecifier",
        local: "A",
      },
      {
        type: "ImportSpecifier",
        imported: "a",
        local: "a",
      },
      {
        type: "ImportSpecifier",
        imported: "b",
        local: "b",
      },
    ] as (ImportSpecifier | ImportDefaultSpecifier)[]);

    // 边界情况
    expect(partial(`import , from "A";`)).toThrowError();
    expect(partial(`import { , a }from "A";`)).toThrowError();
    expect(partial(`import { , } from "A";`)).toThrowError();
    expect(partial(`import , from "A";`)).toThrowError();
    expect(partial(`import A, from "A";`)).toThrowError();
    expect(partial(`import A {} from "A";`)).toThrowError();
    expect(partial(`import A { a, b } from "A";`)).toThrowError();
    expect(partial(`import A from "A";`)).not.toThrowError();
    expect(partial(`import {  } from "A"`)).not.toThrowError();
    expect(partial(`import{}from"A"`)).not.toThrowError();
    expect(partial(` import { } from "A" `)).not.toThrowError();
    expect(partial(`import { a as b, } from "A" `)).not.toThrowError();
    expect(partial(`import {a as b,} from "A" `)).not.toThrowError();
    expect(partial(`import { a as b, b,} from "A" `)).not.toThrowError();
    expect(partial(`import A,{ } from "A"`)).not.toThrowError();
  });

  it("mergeImportDeclarations", () => {
    expect(
      mergeImportDeclarations([
        parseImportDeclaration('import * as A from "A"'),
        parseImportDeclaration('import B from "B"'),
        parseImportDeclaration('import B, { c, d } from "B"'),
        parseImportDeclaration('import { e, f } from "B"'),
        parseImportDeclaration('import { g as g1 } from "B"'),
        parseImportDeclaration('import {  } from "B"'),
        parseImportDeclaration('import C from "C"'),
      ])
    ).toEqual([
      parseImportDeclaration('import * as A from "A"'),
      parseImportDeclaration('import B, { e, f, g as g1 } from "B"'),
      parseImportDeclaration('import B, { c, d } from "B"'),
      parseImportDeclaration('import C from "C"'),
    ] as BaseImportDeclaration[]);

    expect(
      mergeImportDeclarations([
        parseImportDeclaration('import * as A from "A"'),
        parseImportDeclaration('import { a, b } from "A"'),
        parseImportDeclaration('import { c } from "A"'),
      ])
    ).toEqual([
      parseImportDeclaration('import * as A from "A"'),
      parseImportDeclaration('import { a, b, c } from "A"'),
    ]);

    expect(
      mergeImportDeclarations([
        parseImportDeclaration('import { a, b, c0 as c } from "A"'),
        parseImportDeclaration('import { a1 as a, b, c0 as c } from "A"'),
      ])
    ).toEqual([
      parseImportDeclaration('import { a, b, c0 as c, a1 as a } from "A"'),
    ]);
  });
});
