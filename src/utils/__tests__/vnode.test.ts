import { expect, describe, it } from "vitest";
import { identity, upperFirst } from "lodash-es";
import { createVNode, vnodeToString } from "@/utils";

const { JSXPropFormatter, VueTemplatePropFormatter } =
  vnodeToString.propFormatters;

describe("createVNode", () => {
  it("only tag", () => {
    const vnode = createVNode("a");
    expect(vnode.tag).toBe("a");
  });

  it("without props", () => {
    const vnode = createVNode({
      tag: "a",
    });
    expect(vnode.props).toEqual({});
  });

  it("without children", () => {
    const vnode = createVNode({
      tag: "a",
    });
    expect(vnode.children).toEqual([]);
  });

  it("by vnodeLike", () => {
    const vnode = createVNode({
      tag: "a",
      props: {
        a: 1,
        b: false,
      },
      children: [
        createVNode({
          tag: "b",
          props: {
            a: [1],
          },
        }),
      ],
    });
    expect(vnode.props).toEqual({
      a: "1",
      b: "false",
    });
    expect(vnode.children[0].props).toEqual({
      a: "[1]",
    });
  });
});

describe("vnodeToString", () => {
  it("selfClosing", () => {
    const vnode = createVNode("div");
    expect(
      vnodeToString(vnode, {
        tag: {
          format: identity,
        },
        property: {
          format: JSXPropFormatter,
        },
      })
    ).toBeExceptSpaces(`<div></div>`);
    expect(
      vnodeToString(vnode, {
        tag: {
          selfClosing: true,
          format: identity,
        },
        property: {
          format: JSXPropFormatter,
        },
      })
    ).toBeExceptSpaces(`<div/>`);
  });

  it("tag formatter", () => {
    const vnode = createVNode("div");
    expect(
      vnodeToString(vnode, {
        tag: {
          selfClosing: false,
          format: upperFirst,
        },
      })
    ).toBeExceptSpaces(`<Div></Div>`);
  });

  it("sort props", () => {
    const vnode = createVNode({
      tag: "div",
      props: {
        b: 12,
        a: 10,
      },
    });
    const str = vnodeToString(vnode, {
      tag: {
        selfClosing: true,
      },
      property: {
        format: JSXPropFormatter,
        sort(a, b) {
          return a.value > b.value ? 1 : -1;
        },
      },
    });
    expect(str).toBeExceptSpaces(`<div a={10} b={12} />`);
  });

  it("VueTemplatePropertyFormatter", () => {
    const vnode = createVNode({
      tag: "MyApp",
      props: {
        string: "test",
        number: 1,
        boolean: true,
      },
    });
    vnode.props["v-model"] = "obj.foo";
    vnode.props["value.sync"] = "obj.foo";

    expect(
      vnodeToString(vnode, {
        tag: {
          selfClosing: false,
        },
        property: {
          format: VueTemplatePropFormatter,
        },
      })
    ).toBeExceptSpaces(
      '<MyApp string="test" :number="1" boolean v-model="obj.foo" :value.sync="obj.foo"></MyApp>'
    );
  });

  it("JSXPropertyFormatter", () => {
    const vnode = createVNode({
      tag: "MyApp",
      props: {
        string: "test",
        number: 1,
        boolean1: true,
        boolean2: false,
      },
    });
    expect(
      vnodeToString(vnode, {
        property: {
          format: JSXPropFormatter,
        },
      })
    ).toBeExceptSpaces(
      '<MyApp string="test" number={1} boolean1 boolean2={false}></MyApp>'
    );
  });

  it("custom tabs", () => {
    const vnode = createVNode({
      tag: "div",
      children: [createVNode("span")],
    });
    expect(vnodeToString(vnode, { tabs: 0 })).toBe(
      `<div>\n<span></span>\n</div>`
    );
  });

  it("with children", () => {
    const vnode = createVNode({
      tag: "div",
      children: [createVNode("span")],
    });
    expect(vnodeToString(vnode)).toBeExceptSpaces(`<div><span></span></div>`);
  });

  it("without options", () => {
    const vnode = createVNode({
      tag: "diV",
      props: {
        b: 12,
        a: 10,
      },
      children: [createVNode("span")],
    });
    expect(vnodeToString(vnode)).toBe(
      `<diV b="12" a="10">\n  <span></span>\n</diV>`
    );
  });
});
