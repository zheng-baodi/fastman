import { v4 } from "uuid";
import { describe, expect, it } from "vitest";
import {
  stringifyExp,
  generateObjectLiteral,
  generateArrayLiteral,
  generateImportDeclarationCode,
  generateFormObjectLiteral,
} from "../escodegen";
import { parseImportDeclaration } from "@/utils";
import { ParsedFormItem } from "@/types";

describe("escodegen", () => {
  it("stringifyExp", () => {
    expect(stringifyExp("1")).toBe(`"1"`);
    expect(stringifyExp(1)).toBe("1");
    expect(stringifyExp(true)).toBe("true");
    expect(stringifyExp(null)).toBe("null");
    expect(stringifyExp(undefined)).toBe("undefined");
    expect(stringifyExp([1, 2, "3"])).toBe(`[1,2,"3"]`);
    expect(stringifyExp({ a: 1, b: "2" })).toBe(`{"a":1,"b":"2"}`);
  });

  it("generateImportDeclarationCode", () => {
    expect(
      generateImportDeclarationCode({
        source: "A",
        specifiers: [
          {
            type: "ImportNamespaceSpecifier",
            local: "A",
          },
        ],
      })
    ).toBe(`import * as A from "A"`);

    expect(
      generateImportDeclarationCode({
        source: "A",
        specifiers: [],
      })
    ).toBe(`import { } from "A"`);

    expect(
      generateImportDeclarationCode({
        source: "A",
        specifiers: [
          {
            type: "ImportSpecifier",
            imported: "a",
            local: "a",
          },
          {
            type: "ImportSpecifier",
            imported: "b",
            local: "b1",
          },
        ],
      })
    ).toBe(`import { a, b as b1 } from "A"`);

    expect(
      generateImportDeclarationCode({
        source: "A",
        specifiers: [
          {
            type: "ImportDefaultSpecifier",
            local: "A",
          },
        ],
      })
    ).toBe(`import A from "A"`);

    expect(
      generateImportDeclarationCode({
        source: "A",
        specifiers: [
          {
            type: "ImportDefaultSpecifier",
            local: "A",
          },
          {
            type: "ImportSpecifier",
            imported: "a",
            local: "a",
          },
          {
            type: "ImportSpecifier",
            imported: "b",
            local: "b1",
          },
        ],
      })
    ).toBe(`import A, { a, b as b1 } from "A"`);
  });

  it("generateObjectLiteral", () => {
    expect(
      generateObjectLiteral({
        for: "123",
        "ba-r": "[]",
        obj: { age: "10" },
        array: ["1", "[]"],
      })
    ).toBeExceptSpaces(
      `{ for: 123, "ba-r": [], obj: { age: 10 }, array: [1, []] }`
    );

    expect(generateObjectLiteral(["a", "b"])).toBeExceptSpaces("{ a, b }");

    expect(
      generateObjectLiteral([["a", "A"], ["b"], ["c", "C"]])
    ).toBeExceptSpaces("{ a: A, b, c: C }");

    expect(generateObjectLiteral([])).toBeExceptSpaces("{}");
    expect(generateObjectLiteral({})).toBeExceptSpaces("{}");
  });

  it("generateFormObjectLiteral", () => {
    const item1: ParsedFormItem = {
      label: "",
      field: "p1",
      required: false,
      span: 1,
      component: {
        id: v4(),
        name: "test",
        custom: true,
        import: parseImportDeclaration('import A from "A"'),
        initialValue: 10,
        props: {},
      },
      regExps: [],
      validators: [],
    };
    const item2: ParsedFormItem = {
      label: "",
      field: "p2",
      required: false,
      span: 1,
      component: {
        id: v4(),
        name: "test",
        custom: true,
        import: parseImportDeclaration('import A from "A"'),
        props: {},
      },
      regExps: [],
      validators: [],
    };
    expect(generateFormObjectLiteral([item1, item2])).toBeExceptSpaces(
      `{ p1: 10, p2: "" }`
    );
  });

  it("generateArrayLiteral", () => {
    expect(generateArrayLiteral([])).toBe("[]");
    expect(
      generateArrayLiteral(["1", "[]", { age: "10" }, ["1", "[]"]])
    ).toBeExceptSpaces(`[1, [], {age: 10}, [1, []]]`);
  });
});
