export function transformStringAsTemplate(template: string, context: Data) {
  const keys = Object.keys(context);
  const values = keys.map((key) => context[key]);
  const get = new Function(keys.join(","), `return \`${template}\``);
  return get(...values) as string;
}
