export function readStore<T = any>(key: string) {
  const value = localStorage.getItem(key);
  return value ? (JSON.parse(value) as T) : null;
}

export function writeStore(key: string, value: any) {
  localStorage.setItem(key, JSON.stringify(value));
}

export function removeStore(key: string) {
  localStorage.removeItem(key);
}
