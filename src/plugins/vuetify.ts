import { createVuetify } from "vuetify";
import "@mdi/font/css/materialdesignicons.css";
import "vuetify/styles";

const BaseInputProps = {
  density: "compact",
  variant: "outlined",
  noDataText: "暂无数据",
};

// https://vuetifyjs.com/en/introduction/why-vuetify/#feature-guides
export default createVuetify({
  defaults: {
    VTextField: BaseInputProps,
    VSelect: BaseInputProps,
    VRadioGroup: BaseInputProps,
    VCard: {
      ripple: false,
    },
  },
  theme: {
    themes: {
      light: {
        colors: {
          primary: "#1867C0",
          secondary: "#5CBBF6",
        },
      },
    },
  },
});
