/// <reference types="vite/client" />

// import { vitest } from "vitest";

declare interface Data {
  [key: string]: any;
}

declare type DeepRequiredObject<T extends object> = {
  [P in keyof T]-?: NonNullable<T[P]> extends (...args: any) => any
    ? T[P]
    : NonNullable<T[P]> extends object
    ? DeepRequiredObject<NonNullable<T[P]>>
    : T[P];
};

// type OptionalKey<T> = NonNullable<
//   {
//     [K in keyof T]: undefined extends T[K] ? K : never;
//   }[keyof T]
// >;

// declare type KeepOptional<T extends object> = {
//   [K in OptionalKey<T>]?: NonNullable<T[K]> extends object
//     ? KeepOptional<NonNullable<T[K]>>
//     : T[K];
// };
