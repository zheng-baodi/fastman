${imports}
export default {
  name: 'App',
  data() {
    return {
      form: ${formObjLiteral},
      rules: ${ruleObjLiteral}
    }
  },
  // eslint-disable-next-line no-unused-vars
  render(h) {
    return (
    <Form props={{ model: this.form }} rules={this.rules}>
      ${view}
    </Form>
    )
  }
}