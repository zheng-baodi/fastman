<template>
  <el-form :model="form" :rules="rules">
    ${view}
  </el-form>
</template>

<script>
${imports}
export default {
  name: 'App',
  components: ${componentRegisterObjLiteral},
  data() {
    return {
      form: ${formObjLiteral},
      rules: ${ruleObjLiteral}
    }
  }
}
</script>