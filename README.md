# 简介
基于现代前端UI库，生成可用的表单代码。

# 背景
在开发后台管理系统时，需编写大量表单相关的代码，包括但不限于：
1. 视图（jsx或vue-template）
2. 导入二次封装的表单组件
3. 导入输入值验证工具（正则表达式、自定义函数）
4. 将验证输入值工具和视图绑定

这些工作的难度算不上高，但较为繁琐——我们可能会频繁地穿梭在各个文件中复制代码，甚至就地编写它们。

基于此，fastman诞生了，它允许我们以可视化的方式轻松完成上述工作。

# 在线体验
[http://zbd329.top/fastman](http://zbd329.top/fastman)

# 开始使用
需本地运行项目或者打包后部署至服务器上。

fastman以配置文件为核心进行工作，配置文件路径：`/src/public/workspace.json`（[配置文件手册](./samples/workspace.md)）。

示例：
```json
{
  "ui": "element-ui",
  "name": "我的vue2项目",
  "components": [
    {
      "name": "自定义组件A",
      "import": "import MyInput from \"@/components/MyInput\"",
      "initialValue": "",
      "props": {
        "title": "MyInput"
      }
    }
  ],
  "regExps": [
    {
      "name": "字母",
      "import": "import { letterReg } from '@/utils/rule'",
      "message": "${label}必须为字母"
    },
    {
      "name": "数字",
      "import": "import { numberReg } from '@/utils/rule'",
      "message": "${label}必须为数字"
    }
  ],
  "validators": [
    {
      "name": "小于100",
      "import": "import { lessThan100 } from '@/utils/rule'"
    }
  ],
  "templates": [
    ["vue2-jsx", "./templates/jsx.txt"],
    ["vue2-template", "./templates/template.txt"]
  ]
}
```

# 系统总览

## 工作流
![工作流](./samples/workflow.png)

## 系统资源
系统资源分为表单组件和验证工具，其中验证工具又分为正则表达式和自定义函数。

每个系统资源都至少拥有两个属性：`name`和`import`，前者在界面上展示，后者用于生成代码。

示例（js）：
```js 
const 组件资源 = {
  name: "二次封装的表单组件",
  import: "import A from 'A'"
};
const 正则表达式资源 = {
  name: "正则验证1",
  import: "import { reg1 } from '@/utils/reg'"
};
```

## 代码生成模板
一个js模板字符串，但不包含定界符。

fastman会根据用户输入生成若干插值变量，并用这些变量去解析代码生成模板，以此获取最终可用的代码。

假设fastman生成了一个名为`view`的变量，值的含义为视图层代码，并有以下代码生成模板：
```vue
<template>
  ${view}
</template>
<script>
// ...
</script>
```
当`view`的值为`<input v-model="form.name" />`时，最终生成的代码为：
```vue
<template>
  <input v-model="form.name" />
</template>
<script>
// ...
</script>
```
事实上，fastman会生成多个变量供我们使用——这取决于我们选择哪个前端UI库进行工作。

# 文档链接
[配置文件手册](./samples/workspace.md)

[插值变量](./samples/interpolation.md)

[内置组件标识](./samples/tag.md)
