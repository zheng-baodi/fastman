<style>
table th:first-of-type {
  width: 2.5cm;
}
</style>

|UI库|内置组件标识|
|-|-|
|element-ui|Input、Select、RadioGroup、CheckboxGroup、Checkbox、InputNumber、DatePicker、TimeSelect、TimePicker、Autocomplete、Switch、Cascader、Slider、Rate、ColorPicker、Upload、Transfer|
|element-plus|ElInput、ElSelect、EladioGroup、ElCheckboxGroup、ElCheckbox、ElInputNumber、ElDatePicker、ElTimeSelect、ElTimePicker、ElAutocomplete、ElSwitch、ElCascader、ElSlider、ElRate、ElColorPicker、ElUpload、ElTransfer|