# element-ui、element-plus
|变量名|含义|
|-|-|
|view|视图代码（jsx或template）|
|imports|相关导入语句（表单组件、正则、自定义函数）|
|formObjLiteral|表单值绑定对象字面量|
|ruleObjLiteral|表单验证对象字面量|
|componentRegisterObjLiteral|组件注册对象字面量|

view示例：
```vue
<el-row>
  <el-col :span="12">
    <el-form-item label="姓名" prop="name">
      <el-input v-model="form.name" />
    </el-form-item>
  </el-col>
</el-row>
```

imports示例：`import { Input } from 'element-ui'`

formObjLiteral示例：`{ name: "" }`

ruleObjLiteral示例：`{ name: [{ required: true, message: "请输入姓名"}] }`

componentRegisterObjLiteral示例：`{ ElInput: Input }`