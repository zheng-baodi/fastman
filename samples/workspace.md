# 工作空间
一个配置文件代表一个工作空间

# 预定义类型
|类型|说明|
|---|---|
|`Path`|等价于`string`，支持相对路径和绝对路径|
|`TemplateString`|等价于`string`，字符串内容为js模板字符串（不包含定界符）|
|`BuiltInComponentTag`|等价于`string`，UI库的内置组件标识，参见[tag.md](./tag.md)|

# 单工作空间
对象格式的JSON文件

## ui
类型：`enum`，枚举值：`"element-ui"`、`"element-plus"`，是否必需：`是`

说明：不同的值会提供不同的内置表单组件、[插值变量](./interpolation.md)以及[代码生成配置](./codegen.md)

## name
类型：`string`，是否必需：`是`

说明：给工作空间起个名字

## components
类型：`object[]`，是否必需：`否`，默认值：`[]`

说明：定义二次封装的表单组件

|属性|类型|是否必需|默认值|说明|
|:----|:----|:----:|:----:|:----|
|name|string|是|/|/|
|import|string|是|/|一条js导入语句（需至少包含一个变量）|
|initialValue|boolean/string/number/object/array/null|否|""|组件的初始绑定值|
|props|object|否|{}|为组件指定额外的属性|

## regExps
类型：`object[]`，是否必需：`否`，默认值：`[]`

说明：定义正则表达式验证工具

|属性|类型|是否必需|默认值|说明|
|:----|:----|:----:|:----:|:----|
|name|string|是|/|/|
|import|string|是|/|js导入语句（需至少包含一个变量）|
|message|TemplateString|是|/|插值变量：label、field|

## validators
类型：`object[]`，是否必需：`否`，默认值：`[]`

说明：定义自定义函数验证工具

|属性|类型|是否必需|默认值|说明|
|:----|:----|:----:|:----:|:----|
|name|string|是|/|/|
|import|string|是|/|js导入语句（需至少包含一个变量）|

## templates

类型：`Path[]`，是否必需：`是`

说明：代码生成模板的路径数组，至少一个值

示例：[`"./templates/test.txt"`]

## includedBuiltInComponents
类型：`BuiltInComponentTag[]`，是否必需：`否`，默认值：`[]`

说明：标记使用哪些内置组件

示例：`["Input", "Select"]`

## excludedBuiltInComponents

类型：`string[]`，是否必需：`否`，默认值：`[]`

说明：标记不使用哪些内置组件，如果和`includedBuiltInComponents`选项同时被指定，该选项会被忽略

# 多工作空间
数组格式的JSON文件：`Path[]`

说明：配置文件的路径数组，至少一个值

示例：`["./project1/workspace.json", "./project2/workspace.json"]`
